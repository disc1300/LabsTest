//*this is kewl beans, super kewl beans

package OLDLABS.labsFirstHalf;


	import java.util.Arrays;

	public class Lab02
	{
		public static void main(String[] parms)
		{
			question1();
			question2();
			question3();

			System.out.println("\nEnd of processing.");
		}

		public static void question1()
		{
			System.out.println("\nQuestion 1:\n");
			fib(46);
			fib(47);
		}

		public static void fib(int max)
		{
			int fib1, fib2;
			int sum;
			int count;

			fib1 = 1;
			fib2 = 1;
			for (count=3; count<=max; count++)
			{
				sum = fib1 + fib2;
				fib1 = fib2;
				fib2 = sum;
				System.out.print(fib2 +" ");
			}
			System.out.println();
			System.out.println(max +" " +fib2);
		}

		public static void question2()
		{
			System.out.println("\nQuestion 2:\n");
			int[] array1 = {10, 20, 30, 40, 50};
			int[] array2 = {15, 25, 29, 60, 70};
			int[] result;

			result = mergeArrays(array1, array2);

			System.out.println(Arrays.toString(result));
		}

		public static int[] mergeArrays(int[] array1, int[] array2)
		{
			int[] array3;
			int count1;
			int count2;
			int count3;

			array3 = new int[array1.length + array2.length];
			count1 = 0;
			count2 = 0;
			for (count3=0; count1<array1.length && count2<array2.length; count3++)
			{
				if (array1[count1] < array2[count2])
				{
					array3[count3] = array1[count1];
					count1++;
				}
				else
				{
					array3[count3] = array2[count2];
					count2++;
				}
			}

			// This handles the case of elements remaining in array2
			// Similar code is needed to handle the case of elements remaining in array1
	        while (count2<array2.length)
	        {
	            array3[count3] = array2[count2];
	            count2++;
	            count3++;
	        }

			return array3;
		}

		public static void question3()
		{
			StudentTwo[] students;

			System.out.println("\nQuestion 3:\n");

			students = getStudents();

			printStudents(students);
			printAverageGPA(students);
			printHonourStudents(students);
		}

		private static StudentTwo[] getStudents()
		{
			StudentTwo[] students;
	        int count;

	        students = new StudentTwo[Lab2Utility.getNumberStudents()];
	        
	        for (count=0; count<students.length; count++)
	        {
	            students[count] = Lab2Utility.getStudent();
	        }

			return students;
		}

		private static void printStudents(StudentTwo[] students)
		{
			int count;

			System.out.println("Students:");
			for (count=0; count<students.length; count++)
			{
				System.out.println(students[count]);
			}
			System.out.println();
		}

		private static void printAverageGPA(StudentTwo[] students)
		{
			double sumGPA;
			int count;

			sumGPA = 0;
			for (count=0; count<students.length; count++)
			{
				sumGPA += students[count].getGPA();
			}
			System.out.println("The average GPA is " +sumGPA/students.length);
			System.out.println();
		}

		private static void printHonourStudents(StudentTwo[] students)
		{
			int count;

			System.out.println("Honour students:");
			for (count=0; count<students.length; count++)
			{
				if (students[count].isHonourStudent())
				{
					System.out.println(students[count]);
				}
			}
			System.out.println();
		}
	}

	/******************************************************************/
	/******************************************************************/

	class StudentTwo
	{
		private final int numb;
		private final String name;
		private final double hgpa;

		public StudentTwo(int $num, String $name, double fgpa)
		{
			this.numb = $num;   
			this.name = $name;
			this.hgpa = fgpa;
		}

		public double getGPA()
		{
			return hgpa;
		}

		public boolean isHonourStudent()
		{
			return true;
		}

                @Override
		public String toString()
		{
			return numb +" " +name +" " +hgpa;
		}
	}

	/******************************************************************/
	/******************************************************************/

	class Lab2Utility
	{
	    private static int count = 0;

		private static final StudentTwo[] students = new StudentTwo[]
		{
			new StudentTwo(7654321, "Lara Zhivago", 3.75),
			new StudentTwo(7654322, "Betty Brown", 1.9),
			new StudentTwo(7654323, "Xris Xross", 0.5),
			new StudentTwo(7654324, "Dr. Dre", 4.0),
			new StudentTwo(7654325, "Joe Cool", 2.0)
		};

	    public static int getNumberStudents()
	    {
	        return students.length;
	    }

	    public static StudentTwo getStudent()
	    {
	        StudentTwo student;

	        student = null;
	        if (count<students.length)
	        {
	            student = students[count];
	            count++;
	        }
	        return student;
	    }
	}

	/*
	Students:
	7654321 Lara Zhivago 3.75
	7654322 Betty Brown 1.9
	7654323 Xris Xross 0.5
	7654324 Dr. Dre 4.0
	7654325 Joe Cool 2.0

	The average GPA is 2.43

	Honour students:
	7654321 Lara Zhivago 3.75
	7654324 Dr. Dre 4.0

	Removing failing students.

	Students:
	7654321 Lara Zhivago 3.75
	null
	null
	7654324 Dr. Dre 4.0
	7654325 Joe Cool 2.0

	Compacting failing students.

	Students:
	7654321 Lara Zhivago 3.75
	7654324 Dr. Dre 4.0
	7654325 Joe Cool 2.0
	null
	null

	Students:
	7654321 Lara Zhivago 3.75
	7654324 Dr. Dre 4.0
	7654325 Joe Cool 2.0
	*/
